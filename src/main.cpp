/******************************************************************
*  Programa para el control de una maqueta enfocado a la domotica *
*  Esta misma cuenta con leds, switchs, buttons, potentiometers,  *
*  ldr y buzzer.                                                  *
*  Los comandos de control se leen por puerto Serial (Tx, Rx) y se*
*  envia el cambio de estado de los sensores por Serial en formato*
*  JSON.                                                          *
*  {  // READ                                                     *
*     "D2": value(int),                                           *
*     "D3": value(int),                                           *
*     "D4": value(int),                                           *
*     "D5": value(int),                                           *
*     "D6": value(int),                                           *
*     "D7": value(int),                                           *
*     "D8": value(int),                                           *
*     "D9": value(int),                                           *
*     "D10": value(int)                                           *
*  }                                                              *
*  {  // WRITE                                                    *
*     "D21": value(int),                                          *
*     "D20": value(int),                                          *
*     "D19": value(int),                                          *
*     "A4": value(int),                                           *
*     "A3": value(int),                                           *
*     "A2": value(int)                                            *
*  }                                                              *
*                                                                 *
*  Board: Arduino-Nano                                            *
*                                                                 *
*    Author:   Pablo Cardozo                                      *
*    Company:  Electric Eye                                       *
*    Position: Frontend Developer                                 *
*                                                                 *
*******************************************************************
*                      PIN-OUT                                    *
*     light_bathroom:      D2                                     *
*     light_bedroom:       D3                                     *
*     light_main_bathroom: D4                                     *
*     light_garden:        D5                                     *
*     light_living_room:   D6                                     *
*     light_dining_room:   D7                                     *
*     light_kitchen:       D8                                     *
*     light_main_room:     D9                                     *
*     siren:               D10                                    *
*                                                                 *
*******************************************************************
*                      PIN-IN                                     *
*     switch_bedroom:     A7                                      *
*     switch_main_room:   A6                                      *
*     sensor_gas:         A5                                      *
*     sensor_temperature: A4                                      *
*     dimmer_living_room: A3                                      *
*     sensor_light:       A2                                      *
*                                                                 *
******************************************************************/

#include <Arduino.h>
#include <ArduinoJson.h>

// Pin-out
#define pin_light_bathroom       2
#define pin_light_bedroom        3
#define pin_light_main_bathroom  4
#define pin_light_garden         5
#define pin_light_living_room    6
#define pin_light_dining_room    7
#define pin_light_kitchen        8
#define pin_light_main_room      9
#define pin_siren                10
// Pin-in
#define pin_switch_bedroom       A7
#define pin_switch_main_room     A6
#define pin_sensor_gas           A5
#define pin_sensor_temperature   A4
#define pin_dimmer_living_room   A3
#define pin_sensor_light         A2

// Summary
int8_t pinOut[] = { pin_light_bathroom, pin_light_bedroom, pin_light_main_bathroom, pin_light_garden, pin_light_living_room, pin_light_dining_room, pin_light_kitchen, pin_light_main_room, pin_siren };
int8_t pinIn[] = { pin_switch_bedroom, pin_switch_main_room, pin_sensor_gas, pin_sensor_temperature, pin_dimmer_living_room, pin_sensor_light };

// Global variables
StaticJsonDocument<200> read;
StaticJsonDocument<200> write;
bool status_switch_bedroom;
bool status_switch_main_room;
bool status_sensor_gas;
int16_t value_sensor_temperature;
int16_t value_dimmer_living_room;
int16_t value_sensor_light;
int8_t status_change;

void Off () {
  for (int8_t i=0; i < sizeof(pinOut); i++) {
    digitalWrite(pinOut[i], LOW);
  }
}

void ReadSensors () {
  write["A7"] = digitalRead(pin_switch_bedroom);
  write["A6"] = digitalRead(pin_switch_main_room);
  write["A5"] = digitalRead(pin_sensor_gas);
  write["A4"] = analogRead(pin_sensor_temperature);
  write["A3"] = analogRead(pin_dimmer_living_room);
  write["A2"] = analogRead(pin_sensor_light);
  // send sensor values
  serializeJson(write, Serial);
}



void setup() {
  Serial.begin(9600);
  for (int8_t i=0; i < sizeof(pinOut); i++) {
    pinMode(pinOut[i], OUTPUT);
  }
  for (int8_t i=0; i < sizeof(pinIn); i++) {
    pinMode(pinIn[i], INPUT);
  }
  Off();
  // Generate JSON-write
  ReadSensors();
  
  // Serial.println();
  // serializeJsonPretty(read, Serial);
  // Serial.println();
  // serializeJsonPretty(write, Serial);
}

void loop() {
  delay(5000);
  ReadSensors();
  
  char json_read[50];
  int i=0;
  while ( Serial.available() ) {
    json_read[i] = Serial.read();
    i++;
    delay(1);
  }
  DeserializationError error = deserializeJson(read, json_read);
  if (!error) {
    digitalWrite(pin_light_bathroom, read["D2"]);
    digitalWrite(pin_light_bedroom, read["D3"]);
    digitalWrite(pin_light_main_bathroom, read["D4"]);
    digitalWrite(pin_light_garden, read["D5"]);
    digitalWrite(pin_light_living_room, read["D6"]);
    digitalWrite(pin_light_dining_room, read["D7"]);
    digitalWrite(pin_light_kitchen, read["D8"]);
    digitalWrite(pin_light_main_room, read["D9"]);
    digitalWrite(pin_siren, read["D10"]);
  }
}